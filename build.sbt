name := """hello-scala"""

version := "1.0"

scalaVersion := "2.11.7"

val camelVersion = "2.16.2"

libraryDependencies ++=Seq(

  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "org.apache.camel" % "camel-core" % camelVersion,
  "org.apache.camel" % "camel-scala" % camelVersion,
  "org.apache.camel" % "camel-test" % camelVersion,
  "org.apache.camel" % "camel-gson" % camelVersion

)



fork in run := true