import org.apache.camel.impl.DefaultCamelContext

/**
  * Created by tmaus
  */
object App {
  def main(args: Array[String]): Unit = {
    val camel = new DefaultCamelContext()
    camel.addRoutes(new ExampleRouteBuilder(camel))
    camel.start()
    while(true){}
  }
}
