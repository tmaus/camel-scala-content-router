
import com.google.gson.{Gson, JsonObject}
import org.apache.camel.{Exchange, CamelContext}
import org.apache.camel.scala.dsl.builder.ScalaRouteBuilder

/**
  * Created by tmaus
  */
class ExampleRouteBuilder(context: CamelContext) extends ScalaRouteBuilder(context){

  val gson = new Gson

  def processPayload = (e:Exchange) => {
    val jo = gson.fromJson(e.in[String],classOf[JsonObject])

    if (jo.has("route1")) e.getIn.setHeader("route1","true")
    if (jo.has("route2")) e.getIn.setHeader("route2","true")
    if (jo.has("route3")) e.getIn.setHeader("route3","true")
  }

  def transformForRoute1 = (e:Exchange) => {
    // transform payload according to endpoint requirements
  }

  def transformForRoute2 = (e:Exchange) => {
    // transform payload according to endpoint requirements
  }

  def transformForRoute3 = (e:Exchange) => {
    // transform payload according to endpoint requirements
  }

  /**
    * We define our routes here !!!
    */
   from("direct:start") ==> {
     process(processPayload)
     when(_.header("route1")) to("direct:route1")
     when(_.header("route2")) to("direct:route2")
     when(_.header("route3")) to("direct:route3")
   }

  from("direct:route1") ==> {
    transform(transformForRoute1)
    to("mock:route1End")
  }

  from("direct:route2") ==> {
    transform(transformForRoute2)
    to("mock:route2End")
  }

  from("direct:route3") ==> {
    transform(transformForRoute3)
    to("mock:route3End")
  }

}
