import java.util.Properties

import org.apache.camel.{ExchangePattern}
import org.apache.camel.scala.dsl.builder.RouteBuilderSupport
import org.apache.camel.test.junit4.CamelTestSupport
import org.junit.Test
import org.scalatest._

class ExampleRouteBuilderSpec extends CamelTestSupport with Matchers with RouteBuilderSupport {

  override  def createRouteBuilder = new ExampleRouteBuilder(context())

  // any properties you want to inject ?
  override def useOverridePropertiesWithPropertiesComponent() : Properties =  {
    val p:Properties  = new Properties()
    //p.put("foo", "bar")
    p
  }

  @Test
  def testRoute1Ok(): Unit ={
    val b="{\"route1\":\"true\"}"
    val e = getMandatoryEndpoint("direct:start").createExchange(ExchangePattern.InOut)
    e.getIn.setBody(b)
    getMockEndpoint("mock:route1End").expectedMessageCount(1)
    getMockEndpoint("mock:route2End").expectedMessageCount(0)
    getMockEndpoint("mock:route3End").expectedMessageCount(0)
    template.send("direct:start",e)
    assertMockEndpointsSatisfied()
  }

  @Test
  def testRoute1And2Ok(): Unit ={
    val b="{\"route1\":\"true\", \"route2\":\"true\"}"
    val e = getMandatoryEndpoint("direct:start").createExchange(ExchangePattern.InOut)
    e.getIn.setBody(b)
    getMockEndpoint("mock:route1End").expectedMessageCount(1)
    getMockEndpoint("mock:route2End").expectedMessageCount(1)
    getMockEndpoint("mock:route3End").expectedMessageCount(0)
    template.send("direct:start",e)
    assertMockEndpointsSatisfied()
  }

  @Test
  def testRoute1And2And3Ok(): Unit ={
    val b="{\"route1\":\"true\", \"route2\":\"true\", \"route3\":\"true\"}"
    val e = getMandatoryEndpoint("direct:start").createExchange(ExchangePattern.InOut)
    e.getIn.setBody(b)
    getMockEndpoint("mock:route1End").expectedMessageCount(1)
    getMockEndpoint("mock:route2End").expectedMessageCount(1)
    getMockEndpoint("mock:route3End").expectedMessageCount(1)
    template.send("direct:start",e)
    assertMockEndpointsSatisfied()
  }

}
